var inProgress = false;

chrome.runtime.onInstalled.addListener(function() {
  // Replace all rules ...
  chrome.declarativeContent.onPageChanged.removeRules(undefined, function() {

    // With a new rule ...
    chrome.declarativeContent.onPageChanged.addRules([
      {
        // That fires when a page's URL contains a 'g' ...
        conditions: [
          new chrome.declarativeContent.PageStateMatcher({
            pageUrl: { urlMatches: '(docs|box|docs.win95.dev)\.mail.ru' },

          })
        ],
        // And shows the extension's page action.
        actions: [ new chrome.declarativeContent.ShowPageAction() ]
      }
    ]);
  });
});

chrome.pageAction.onClicked.addListener(function(tab){
  var 
    a =  document.createElement('a'), 
    wrapper = document.createElement('div'),
    parts,
    xhr,
    docId;
  a.href = tab.url;
  parts = a.pathname.split('/');

  if (!inProgress) {
    inProgress = true;
    chrome.pageAction.setIcon({path: "loading.png", tabId: tab.id});
    
    if (parts.length >= 5 && parts[1] === 'words' && (parts[3] === 'edit' || parts[3] === 'view')) {
      xhr = new XMLHttpRequest();
      xhr.open('GET', a.protocol + a.hostname + ':' + a.port + '/admin/docs/?by=hash&filter=' + parts[2], true);
      xhr.onreadystatechange = function() {
        chrome.pageAction.setIcon({path: "logo.png", tabId: tab.id});
        if (xhr.readyState == 4) {
          wrapper.innerHTML = xhr.responseText; 
          docId = wrapper.querySelectorAll('tr > td')[0].innerHTML;
          chrome.tabs.create({ url: a.hostname + ':' + a.port + '/admin/docs/' + docId + '/', active: false });
        }
        inProgress = false;
        
      }
      xhr.send();
    } else {
      chrome.pageAction.setIcon({path: "logo.png", tabId: tab.id});
      chrome.tabs.create({ url: a.hostname + ':' + a.port + '/admin/', active: false }); 
      inProgress = false;
    }
    
  }

  
});
